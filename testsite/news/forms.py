from django import forms
from .models import News
from django.core.exceptions import ValidationError
import re

class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        # fields = '__all__'
        fields = ['title', 'content', 'is_published', 'category']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'content': forms.Textarea(attrs={'class': 'form-control','rows':2,}),
            'category': forms.Select(attrs={'class': 'form-control'}),
        }

    def clean_title(self):
        title = self.cleaned_data['title']
        if re.match(r'\d', title):
            raise ValidationError('Название не должно начинаться с цифры')
        return title




        #     title = forms.CharField(max_length=150, label='Название вашей новости',
        #                             widget=forms.TextInput(attrs={'class': 'form-control'}))
        # content = forms.CharField(label='Содержание вашей новости', widget=forms.Textarea(attrs={
        #     'class': 'form-control',
        #     'rows': 2,
        # }))
        # # photo = forms.ImageField("Фото", upload_to='photos/%Y/%m/%d/', blank=True)
        # is_published = forms.BooleanField(label='Черновик', required=False, initial=True)
        # category = forms.ModelChoiceField(queryset=Category.objects.all(), empty_label='--Выберите категорию--',
        #                                   label='Категория вашей новости',
        #                                   widget=forms.Select(attrs={'class': 'form-control'}))
