from django.db import models
from django.urls import reverse


class News(models.Model):
    title = models.CharField("Наименование", max_length=150)
    content = models.TextField("Содержание", blank=True)
    created_at = models.DateTimeField("Опубликовано", auto_now_add=True)
    update_at = models.DateTimeField("Изменено", auto_now=True)
    photo = models.ImageField("Фото", upload_to='photos/%Y/%m/%d/', blank=True)
    is_published = models.BooleanField("Черновик", default=True)
    category = models.ForeignKey('Category', verbose_name='Категория', on_delete=models.PROTECT, null=True)

    def get_absolute_url(self):
        return reverse('views_news', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"
        ordering = ["-created_at"]


class Category(models.Model):
    title = models.CharField('Название категории', max_length=150, db_index=True)

    def get_absolute_url(self):
        return reverse('category', kwargs={'category_id': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
        ordering = ["title"]
