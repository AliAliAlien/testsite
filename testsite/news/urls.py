from django.urls import path,include

from .views import *

urlpatterns = [
    path('', HomeNews.as_view(), name='home'),
    # path('', index, name='home'),
    path('category/<int:category_id>/', CategoryNews.as_view(), name='category'),
    # path('category/<int:category_id>/', get_category , name='category'),
    path('news/<int:pk>/', ViewsNews.as_view(), name='views_news'),
    # path('news/<int:news_id>/', views_news , name='views_news'),
    path('add-news/', CreateNews.as_view(), name='add_news'),
    # path('add-news/', add_news , name='add_news'),
    path('__debug__/', include('debug_toolbar.urls')),
]
