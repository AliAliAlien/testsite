from django.shortcuts import render,get_object_or_404, redirect
from django.views.generic import ListView, DetailView , CreateView

from .models import News, Category
from .forms import NewsForm

class HomeNews(ListView):
    model = News
    template_name = 'news/index.html' # Используй конвенции Django чтобы не писать это
    context_object_name = 'news'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Главная страница'
        return context

    def get_queryset(self):
        return News.objects.filter(is_published=False).select_related('category')

# def index(request):
#     news = News.objects.all()
#     context = {
#         'news' : news,
#         'title' : 'Список новостей',
#     }
#     return render(request, 'news/index.html', context=context)

class CategoryNews(ListView):
    model = News
    template_name = 'news/index.html'
    context_object_name = 'news'
    allow_empty = False

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = Category.objects.get(pk=self.kwargs['category_id'])
        return context

    def get_queryset(self):
        return News.objects.filter(is_published=False,category_id=self.kwargs['category_id']).select_related('category')

# def get_category(request, category_id):
#     news = News.objects.filter(category_id=category_id)
#     category = Category.objects.get(pk=category_id)
#     return render(request, 'news/category.html', {'news': news, 'category': category})

# def views_news(request, news_id):
#     news_item = get_object_or_404(News,pk=news_id)
#         #News.objects.get(pk=news_id)
#     return render(request,'news/views_news.html',{'news_item':news_item} )

class ViewsNews(DetailView):
    model = News
    template_name = 'news/views_news.html'
    context_object_name = 'news_item'
    # pk_url_kwarg ='news_id'


class CreateNews(CreateView):
    form_class = NewsForm
    template_name = 'news/add_news.html'


# def add_news(request):
#     if request.method == 'POST':
#         form = NewsForm(request.POST)
#         if form.is_valid():
#             #news = News.objects.create(**form.cleaned_data)
#             news = form.save()
#             return redirect(news)
#     else:
#         form = NewsForm()
#     return render(request, 'news/add_news.html',{'form':form})